﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;

namespace Extractor
{
    public partial class MainWindow : Window
    {
        ExtractorViewModel _model;

        public MainWindow() {
            InitializeViewModel();
            InitializeFilesFromArgs();
            InitializeComponent();
        }

        private void InitializeFilesFromArgs() {
            List<string> realArgs = Environment.GetCommandLineArgs()
                .Skip(1)
                .ToList();



            _model.Files.AddRange(realArgs);
        }

        private void InitializeViewModel() {
            _model = new ExtractorViewModel();
            this.DataContext = _model;
        }

        private void WhenDroppedOnFilesListBox(object sender, DragEventArgs e) {
            if (!CtrlKeyIsPressed(e)) {
                ReplaceFilesWithFiles(e);
            }
            else {
                AddFilesToFiles(e);
            }
        }

        private void AddFilesToFiles(DragEventArgs e)  /* verified */  {
            List<string> files = AnyDraggedFiles(e);
            _model.Files.AddRange(files);
        }

        private void ReplaceFilesWithFiles(DragEventArgs e)  /* verified */  {
            List<string> files = AnyDraggedFiles(e);
            _model.Files = new ObservableCollection<string>(files);
        }

        private List<string> AnyDraggedFiles(DragEventArgs e)  /* verified */  {
            if (!e.Data.GetDataPresent(DataFormats.FileDrop)) {
                return new List<string>();
            }

            object raw = e.Data.GetData(DataFormats.FileDrop);

            List<string> files = new List<string> { };

            if (raw is object[] items) {
                files = items
                    .Select(x => x as string)
                    .ToList();
            }

            files = files
                .Where(x => File.Exists(x))
                .ToList();

            return files;
        }

        private void WhenDraggedOverFilesListBox(object sender, DragEventArgs e)  /* verified */  {
            if (!CtrlKeyIsPressed(e)) {
                e.Effects = DragDropEffects.Move;
            }
            else {
                e.Effects = DragDropEffects.Copy;
            }
        }

        private bool CtrlKeyIsPressed(DragEventArgs e)  /* verified */  {
            return (e.KeyStates & DragDropKeyStates.ControlKey) == DragDropKeyStates.ControlKey;
        }

    }
}
