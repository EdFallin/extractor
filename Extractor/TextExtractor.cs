﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Extractor
{
    public class TextExtractor
    {
        public string Extract(string text, string regex, bool acrossLines) {
            // .Singleline means "treat all the lines as though 
            // they're a single line" — that is, "ignore lines".
            RegexOptions options = acrossLines ? RegexOptions.Singleline : RegexOptions.None;

            MatchCollection raw = Regex.Matches(text, regex, options);

            List<string> matches = new List<string>();

            foreach (Match match in raw) {
                matches.Add(match.Value);
            }

            string output = string.Join(Environment.NewLine, matches);
            return output;
        }
    }
}
