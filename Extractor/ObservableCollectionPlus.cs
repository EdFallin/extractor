﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Extractor
{
    public static class ObservableCollectionPlus
    {
        public static void AddRange<T>(this ObservableCollection<T> self, IEnumerable<T> range)  /* verified */  {
            foreach (T item in range) {
                self.Add(item);
            }
        }
    }
}
