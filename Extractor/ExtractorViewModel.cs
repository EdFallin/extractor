﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.ComponentModel;


namespace Extractor
{
    using IMutable = INotifyPropertyChanged;

    public class ExtractorViewModel : IMutable
    {
        #region Definitions

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion Definitions


        #region Fields

        private TextExtractor _extractor;
        private bool _nowExtracting;

        private ObservableCollection<string> _files = new ObservableCollection<string>();

        private bool _didThrow;

        #endregion Fields


        #region Properties

        public Command ExtractCommand { get; set; }
        public Command ExitCommand { get; set; }

        public string ToMatch { get; set; }
        public bool MatchAcrossLines { get; set; }

        public ObservableCollection<string> Files {
            get {
                return _files;
            }
            set {
                _files = value;
                OnPropertyChanged(nameof(Files));
            }
        }

        public bool DidThrow {
            get => _didThrow;
            set {
                _didThrow = value;
                OnPropertyChanged(nameof(DidThrow));
            }
        }

        #endregion Properties


        #region IMutable

        public void OnPropertyChanged(string name) {
            if (PropertyChanged != null) {
                PropertyChangedEventArgs args = new PropertyChangedEventArgs(name);
                PropertyChanged(this, args);
            }
        }

        #endregion IMutable


        #region Constructors

        public ExtractorViewModel() {
            _extractor = new TextExtractor();

            ExtractCommand = new Command(CanExtract, ExtractFromFiles);
            ExitCommand = new Command(CanExit, ExitApplication);
        }

        #endregion Constructors


        #region Command implementation methods

        private bool CanExtract(object _) {
            bool filesArePresent = Files.Count > 0;
            bool regexIsPresent = !string.IsNullOrWhiteSpace(ToMatch);

            return filesArePresent
                && regexIsPresent
                && !_nowExtracting;
        }

        private void ExtractFromFiles(object _) {
            // Setting button and style states.
            _nowExtracting = true;
            DidThrow = false;

            // Actually extracting, for each file in turn.
            try {
                foreach (string file in Files) {
                    string text = File.ReadAllText(file);
                    string extracted = _extractor.Extract(text, ToMatch, MatchAcrossLines);

                    string output = OutputFileName(file);
                    File.WriteAllText(output, extracted);
                }
            }
            catch {
                DidThrow = true;
            }

            _nowExtracting = false;
        }

        private string OutputFileName(string file) {
            string path = Path.GetDirectoryName(file);
            string name = Path.GetFileNameWithoutExtension(file);
            string ext = Path.GetExtension(file);

            name = $"Extracted from { name } at {DateTime.Now:MM-dd-yy @ hh-mm-ss}";

            string output = Path.Combine(path, name);
            output = Path.ChangeExtension(output, ext);

            return output;
        }

        private bool CanExit(object _) {
            return !_nowExtracting;
        }

        private void ExitApplication(object _) {
            App.Current.Shutdown();
        }

        #endregion Command implementation methods
    }
}
